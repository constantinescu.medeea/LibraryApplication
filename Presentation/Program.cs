﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Simplify.Windows.Forms;

namespace Presentation
{
    class Program
    {
        static void Main(string[] args)
        {
            Presentation.EnableVisualStyles();
            Application.Run(new Form1());
        }
    }
}
