﻿using BookApplication.Dao.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static HttpClient client = new HttpClient();

        static void ShowBook(Book book)
        {
            Console.WriteLine($"Title: {book.Title}\tAuthor: " +
                $"{book.Author}");
        }

        static async Task<Uri> CreateBookAsync(Book book)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "api/books", book);
            response.EnsureSuccessStatusCode();

            // return URI of the created resource.
            return response.Headers.Location;
        }

        static async Task<Book> GetBookAsync(string path)
        {
            Book book = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                book = await response.Content.ReadAsAsync<Book>();
            }
            return book;
        }

        static async Task<Book> UpdateBookAsync(Book book)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync(
                $"api/books/{book.Id}", book);
            response.EnsureSuccessStatusCode();

            // Deserialize the updated product from the response body.
            var bookReturn = await response.Content.ReadAsAsync<Book>();
            return bookReturn;
        }

        static async Task<HttpStatusCode> DeleteBookAsync(string id)
        {
            HttpResponseMessage response = await client.DeleteAsync(
                $"api/books/{id}");
            return response.StatusCode;
        }

        static void Main()
        {
            RunAsync().GetAwaiter().GetResult();
        }

        static async Task RunAsync()
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri("http://localhost:62440/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                // Create a new book
                Book book = new Book
                {
                    Title = "Gizmo",
                    Author = "JK"
                };

                var url = await CreateBookAsync(book);
                Console.WriteLine($"Created at {url}");

                // Get the book
                book = await GetBookAsync(url.PathAndQuery);
                ShowBook(book);

                // Update the book
                Console.WriteLine("Updating price...");
                book.Title = "Titlu nou";
                await UpdateBookAsync(book);

                // Get the updated book
                book = await GetBookAsync(url.PathAndQuery);
                ShowBook(book);

                /* // Delete the book
                 var statusCode = await DeleteBookAsync(book.Id);
                 Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");*/
                Console.ReadLine();

            }
            catch (Exception e)
    {
        Console.WriteLine(e.Message);
    }}
    }
}
