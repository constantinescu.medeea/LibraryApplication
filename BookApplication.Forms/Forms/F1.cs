﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookApplication.Forms.Forms
{
    public Button button1;

    public class F1 : Form
    {
        BooksController booksController;

        public F1(BookController book)
        {
            booksController = book;
        }

        public F1()
        {
            button1 = new Button();
            button1.Size = new Size(40, 40);
            button1.Location = new Point(30, 30);
            button1.Text = "Click me";
            this.Controls.Add(button1);
            button1.Click += new EventHandler(button1_Click);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello World");
        }
    }   
}
