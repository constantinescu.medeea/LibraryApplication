﻿using BookApplication.Dao.IRepositories;
using BookApplication.Dao.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookApplication.Dao.Repositories
{
    public class BookRepository : IBookRepository
    {
        protected BookDbContext _dbContext;
        protected DbSet<Book> _dbSet;

        public BookRepository(BookDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<Book>();
        }

        public void Add(Book book)
        {
            _dbSet.Add(book);
        }

        public virtual void Delete(object id)
        {
            Book bookToDelete = _dbSet.Find(id);
            Delete(bookToDelete);
        }

        public void Delete(Book book)
        {
            if (_dbContext.Entry(book).State == EntityState.Detached)
            {
                _dbSet.Attach(book);
            }

            _dbSet.Remove(book);
        }

        public IQueryable<Book> GetAll()
        {
            return _dbSet;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges(true);
        }

        public Book Update(Book entityToUpdate)
        {
            var entry = _dbContext.Entry<Book>(entityToUpdate);

            if (entry.State == EntityState.Detached)
            {
                var set = _dbContext.Set<Book>();
                var pkey = entityToUpdate.GetType().GetProperty("Id").GetValue(entityToUpdate);
                Book attachedEntity = set.Find(pkey);  // You need to have access to key

                if (attachedEntity != null)
                {
                    var attachedEntry = _dbContext.Entry(attachedEntity);
                    attachedEntry.CurrentValues.SetValues(entityToUpdate);
                }
                else
                {
                    entry.State = EntityState.Modified; // This should attach entity
                }
            }

            _dbContext.SaveChanges();

            return entityToUpdate;
        }

        /*public Book UpdateBook(Book entityToUpdate)
        {
            var entry = _dbContext.Entry<Book>(entityToUpdate);

            //entry.State = EntityState.Modified;

            _dbContext.SaveChanges();

            return entityToUpdate;
        }*/


        public Book GetById(int id)
        {
            var book = _dbSet.Where(b => b.Id == id).FirstOrDefault();

            return book;
        }
    }
}
